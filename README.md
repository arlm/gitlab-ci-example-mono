# GITLAB-CI EXAMPLE: MONO

[![pipeline status](https://gitlab.com/tobiaskoch/gitlab-ci-example-mono/badges/master/pipeline.svg)](https://gitlab.com/tobiaskoch/gitlab-ci-example-mono/commits/master)

---

This is a simple [gitlab continuous integration](https://about.gitlab.com/features/gitlab-ci-cd/) example project (compatible with the [shared runner](https://docs.gitlab.com/runner/) provided on [gitlab.com](https://gitlab.com)) using the official [mono docker image](https://hub.docker.com/_/mono/) to build a visual studio project.

Thanks to the [Tango Desktop Project](http://tango.freedesktop.org) for the repository icon.

## Contributing
see [CONTRIBUTING.md](https://gitlab.com/tobiaskoch/gitlab-ci-example-mono/blob/master/CONTRIBUTING.md)

## Contributors
see [AUTHORS.txt](https://gitlab.com/tobiaskoch/gitlab-ci-example-mono/blob/master/AUTHORS.txt)

## License
**gitlab-ci-example-mono** © 2018  Tobias Koch. Released under the [MIT License](https://gitlab.com/tobiaskoch/gitlab-ci-example-mono/blob/master/LICENSE.md).